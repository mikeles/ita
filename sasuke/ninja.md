La leggenda di Sasuke Sarutobi
------------------------------------------------

![sasuke](sasuke5.jpg)

L'anime-fan dei giorni nostri, quando sente il nome Sasuke, pensa probabilmente a Sasuke Uchiya di Naruto e al suo Sharingan e lì si ferma. In realtà dietro al nome Sasuke c'è una tradizione secolare.

Per i giapponesi, il nome è associato a Sasuke Sarutobi, uno dei Dieci
di Sanada, caduto nell'assedio di Osaka del 1615, in battaglia contro
le forze di Ieyasu Tokugawa, almeno secondo la leggenda dell'epoca
Meji. La leggenda sembrerebbe ispirata alle gesta di un ninja
realmente esistente, che qualcuno identifica con Kozuki Sasuke e altri
con Sarutobi Nisuke. Anche se l'attendibilità storica della figura di
Sasuke è dubbia, questo non ha impedito la formazione di centinaia di
opere ispirate al personaggio. Si può dire che il giovane ninja Sasuke
sia sempre stato famoso. Se ci restringiamo agli ultimi cento anni, va
ricordato che tra il 1911 e il 1925 Bunko Tachikawa scrisse una serie
di libri per ragazzi con protagonista Sasuke che ebbero un grandissimo
successo e da cui vennero tratti ben tre film muti, nel 1918, 1919 e
1922, più un altro film nel 1938 (*Ninjutsu Sekigahara Sarutobi
Sasuke*). Sasuke era anche un personaggio ricorrente del kamishibai,
il teatrino dei cantastorie giapponesi e la sua popolarità non si è
mai affievolita, neppure durante la guerra.

![kamishibai](kamishibai.jpg)

Dopo la seconda guerra mondiale, le opere sui ninja subirono un
rallentamento forzato. Il motivo è che gli Stati Uniti occuparono il
Giappone con 400,000 soldati e posero una forte censura su tutte le
opere di genere storico (libri, fumetti, teatro, film) con la scusa
che inneggiavano al nazionalismo. Ma anche nel periodio più nero c'era
sempre qualche film sul nostro eroe, come *Sarutobi Sasuke Ninjutsu
Senichiya* del 1947 e [Sarutobi Sasuke Senjogadake no
Himatsuri](http://vintageninja.net/?tag=sarutobi-sasuke) del 1950. Nel
1951 le truppe del generale MacArthur iniziarono progressivamente ad
abbandonare il Giappone, la censura venne abolita e ci fu un nuovo
boom di opere ad ambientazione storica. È in quegli anni che scoppia
la febbre dei ninja, probabilmente proprio come un ritorno
alla tradizione giapponese in risposta all'occidentalizzazione forzata
imposta dagli americani. Sasuke Sarutobi e' tra i primi personaggi a
tornare, a partire dal manga di Shigeru Sugiura.

![sasuke-1954](sasuke-1954.png)

Il ninja-boom fa sì che Sasuke torni al cinema, sia in animazione (con
il film Toei [Shonen Sarutobi
Sasuke](https://www.animeclick.it/anime/1639/shonen-sarutobi-sasuke)
del 1959) che in live action (con il film *Sarutobi Sasuke* del 1966,
in occidente distributo in occidente nel 1976 con il titolo di *Ninja
Spy*). La parte del leone la svolgono i manga, iniziando con [Io sono
Sasuke Sarutobi!](https://www.animeclick.it/manga/11888/kagemaru-den-la-leggenda-di-un-ninja) (1960) del Dio dei Manga Osamu Tezuka, subito seguito dal
*Sasuke* di Sampei Shirato (1961).

![sasuke](https://www.animeclick.it/prove/img_tmp/201710/IoSonoSasukeSarutobi-cover-hazard.jpg)

La prima serie d'animazione sui ninja è [Fujimaru
of the Wind: The Childhood of a Ninja](https://www.animeclick.it/anime/4369/shounen-ninja-kaze-no-fujimaru) del 1964, tratta da un manga sempre di
Sanpei Shirato, in cui il padre del protagonista si chiama proprio
Sasuke. Si può dire che in Giappone Sasuke sia il nome ninja per
eccellenza, e sarebbe impossibile fare una lista delle opere in cui
compare tale nome. Tanto per fare un esempio recente, anche in Naruto viene
esplicitemente detto che Sasuke Uchiya viene chiamato così in onore di
Sasuke Sarutobi, padre di Hirokun Sarutobi, il Terzo Hokage.

![sasuke-1961](sasuke-1961.jpg)

Storia dei ninja
---------------------------

La storia dei ninja in Giappone è lunga e ricca misteri. Secondo il
manoscritto "Shinobi Hiden" (1646) le tecniche segrete del ninjitsu
vennero portate in Giappone dalla Cina nel settimo secolo dopo
Cristo. La leggenda narra che Xu Fu, alla ricerca dell'elisir della
vita sui monti Kumano, non potendo trovarlo si stabilì in Giappone e
tramandò le sue tecniche ai figli che successivamente si stabilirono
sulle montagne di Iga. Siccome il manoscritto è stato redatto
mille anni dopo gli eventi narrati non può essere preso troppo
seriamente, ma gli storici credono comunque che l'origine cinese delle tecniche
ninja sia probabile, perché ci sono molti documenti attestanti
l'uso di tecniche di spionaggio di tipo ninja nell'antica Cina.

La parola parola "shinobi" (che si scrive con gli stessi ideogrammi di
"ninja") è attestata nella raccolta di poemi Man'yōshū dell'ottavo
secolo e ci sono documenti attestanti l'esistenza di quello che oggi
chiameremmo ninja nel Giappone del dodicesimo secolo. Il termine
"shinobi no mono" era di uso comune già nel quattordicesimo secolo e
le imprese dei ninja erano ben note. Il boom degli exploit
dei ninja si registra nell'epoca Sengoku, quando la richiesta delle
loro abilità era massima. Nei due secoli di pace forzata dei Tokugawa
il numero di ninja andò assottigliandosi, non essendoci molto lavoro,
e si possono considerare scomparsi a partire dall'epoca della
restaurazione Meiji (1868) anche se ci sono persone che hanno
continuato a spacciarsi per discendenti dei ninja anche in epoca molto
posteriori, fino ad arrivare al mito dei ninja impiegati durante
la seconda guerra mondiale.

I manuali ninja (i famosi "scrolls") esistono veramente, risalgono
all'epoca Sengoku/Tokugawa e si riferiscono a tecniche che già
all'epoca erano considerate vecchie di secoli. I manuali più antichi,
celebri e storicamente affidabili sono il Bansenshukai, lo Shinobi
Hiden della famiglia Hattori e lo Shoninki della famiglia Natori.

Tra le abilità storiche dei ninja ci sono l'abilità nel travestimento,
la capacità di scalare muri, montagne ed alberi, l'abilità di
mimetizzarsi, l'abilità di infiltrarsi in castelli iperprotetti e di
incendiarli, l'abilità di usare gas velenosi e tante altre. Naturalmente
le abilità più fantasiose dei ninja vanno prese *cum grano salis*:
per esempio l'abilità di camminare sull'acqua sembra derivare semplicemente
da un errore di interpretazione: le cosiddette "scarpe per camminare sull'acqua"
non erano scarpe ma semplicemente dei salvagenti!

![water spider](water-spider.png)

È storicamente accertato che nel'anno 1487 delle squadre ninja di
Koga e Iga riuscirono ad infiltrarsi nel campo dello shogun e a
ferirlo, tanto che morì l'anno successivo. Da questo evento discese la
fama dei ninja di queste province, tanto che per qualche secolo il
termine "shinobi no mono" venne sostituito da "Koga no mono" oppure
"Iga no mono". Il nostro Sasuke era un ninja di Koga,
ma la reputazione di Iga era pari o addirittura superiore, a seconda
delle fonti consultate.

In occidente la ninja-mania è una caratteristica degli anni
ottanta. Era semplicemente una conseguenza del successo dei film di
arti marziali degli anni settanta, che erano entrato in tutti media in
tutte le forme, comprese anche quella dei ninja. Qui potrei citare il
romanzo "Ninja" di Eric van Lustbader (1980) oppure la celebre ninja
Elektra Natchios, introdotta nei comics di Daredevil da Frank Miller
nel 1981, oppure la serie TV americana "The Master" (1984) in cui Lee
Van Cleef interpreta un anziano maestro ninja, ma gli esempi sarebbero
un'infinità.

In Italia i ninja divennero popolari tra i bambini proprio grazie
all'anime di [Sasuke il piccolo
Ninja](https://www.animeclick.it/anime/845/manga-sarutobi-sasuke),
trasmesso sulle TV private a partire dal 1981. In Giappone l'anime
andò in onda a partire dal 3 settembre
1968 e quindi ha gia' raggiunto il mezzo secolo di vita.

La serie del 1968
-----------------------------

![sasuke](sasuke7.jpg)
![sasuke](sasuke4.jpg)

La serie del 1968 è ricordata in Italia in quanto prima serie ninja
mai apparsa in TV e anche per la [celebre sigla dei Cavalieri del
Re](https://www.youtube.com/watch?v=De_KEJYYVyw).  Anche alla prima
visione, nel 1981, la serie appariva molto datata, con una grafica,
dei colori, delle musiche e delle animazioni arretrate di un decennio
rispetto alle serie che venivano trasmesse in Italia all'epoca. Ciò
nonostante, Sasuke riusciva a piacere come e più di altre serie
tecnicamente più elaborate, per l'originalità, per l'inedita
ambientazione nel Giappone medievale, per le spiegazioni
dettagliatissime e pseudoscientifiche dei trucchi ninja impiegati da
Sasuke, e soprattutto per la simpatia del personaggio principale. I
bambini dell'epoca si divertivano a correre in cerchio come dei folli
cercando di sdoppiarsi come faceva Sasuke (quarant'anni prima del
*Kage Bunshin* di Naruto!).

Chi vuole accingersi alla visione di Sasuke farebbe comunque bene a
provvedersi di fazzoletti. Nonostante la serie si rivolga a un
pubblico infantile, Sasuke è un vero figlio degli anni sessanta, e non
mancano morti, esplosioni, squartamenti e tristezza a palate.
"Il piccolo ninja" inizia fin da subito con la morte della
madre di Sasuke. Il motivo è che il clan dei Sarutobi è stato
condannato a morte dallo shogun Tokugawa e viene perseguitato dalla
famiglia Hattori. La famiglia Hattori è storicamente esistita e il suo
primo e più celebre esponente è Hattori Hanzo (1541-1596), braccio
armato di Ieyasu Tokugawa e secondo la tradizione ninja di Iga, mentre
Sasuke era un ninja di Koga. Hattori Hanzo compare in una gran quantità
di manga e anime; qui citerò soltanto la serie nota in Italia come
[Nino il mio amico
ninja](https://www.animeclick.it/anime/7570/ninja-hattori-kun) (1981)
che in originale s'intitolata "Ninja Hattori-kun" (Nino è la parodia
di Hattori Hanzo) ed è tratta dal un manga di Fujiko Fujio del 1964.

La serie si svolge in 29 episodi durante i quali Sasuke vive mille
avventure e battaglie, a volte in solitario e a volte assistito dal
padre Daisuke che si nasconde sotto le spoglie di un mendicante.
Meritano una menzione anche i cuginetti di Sasuke, del tutto identici
a lui. La serie e' risolta a un pubblico infantile, ma cio' nonostante
sono presenti messaggi e situazioni molto adulte. Per esempio nelle
ultime puntate si affrontano temi che sono diventati molto importanti
in questi tempi il cui il divorzio è così diffuso. Infatti il padre di
Sasuke vuole risposarsi con una donna che ha una figlia più grande di
Sasuke e questo matrimonio genera parecchi conflitti interni:
chiaramente Sasuke non riesce ad accettare l'amante del padre come sua
nuova madre, fugge di casa e deve passare del tempo primo che la
situazione si sistemi.

Nelle stesse puntate Sasuke e suoi padre cercano di salvare i
cristiani nascosti, giapponesi convertiti al cristianesi e
perseguitati dal governo Tokugawa. E' noto l'eccidio dei cristiani
giapponesi durante la ribellione di Shimabara (1637–1638)) quando il
governo Tokugawa decapitò approssimativamente 37000 persone. Dopo
questo fatto i cristiani continuerano a professare la loro sede in
segreto per più di duecento anni, fino alla riapertura del paese agli
occidentali: anche questa è una storia che meriterebbe di essere più
conosciuta.

Le opere di Shirato sono quasi dei libri di storia, e i suoi ninja
sono i maggiori rappresentanti della tradizione
"realistica" dei ninja: hanno si' abilità eccezionali ma non
completamente assurde. La tradizione realistica si contrappone alla
tradizione "favolosa" di Yamada, i cui ninja hanno capacità
inverosimili, come degli strabilianti poteri oculari, anche se nei
libri Yamada si sforza di darne qualche spiegazione
fantascientifica. Film come il celebre [Ninja Scroll]() del 1993, di
cui abbiamo festeggiato il 25esimo anniversario poco tempo fa,
derivano direttamente dall'opera di Yamada, ed è questa la tradizione
ninja più popolare al momento (non dimentichiamo che il 2018 è stato
l'anno della [seconda serie di
Basilisk](https://www.animeclick.it/anime/21270/basilisk-ouka-ninpouchou)
), per la maggiore spettacolarità e sense of wonder. Eppure la
spettacolarità non è tutto e una serie di cinquant'anni fa, con
animazione poverissime, scelte cromatiche imbarazzanti (a tratti
sembra addirittura una serie in bianco e nero) e un character design
preistorico (mitici i piedi di Sasuke che si riducolo al solo alluce!)
ha anche al giorno d'oggi un fascino innegabile. È una grande serie ed
è la perfetta introduzione all'altro grande capolavoro di Shirato,
[Ninja Kamui](https://www.animeclick.it/anime/3196/ninja-kamui).

Non vi private di questa esperienza solo perché la serie sembra datata:
e' proprio il suo essere datata il suo punto di forza. Seguite la serie di
Sasuke se volete una full immersion nel medioevo giapponese, con tante di
musiche tradizionali e un'iconografia che sembrano uscite da un remoto
passato.

![sasuke11](sasuke11.png)

Altre serie con Sasuke Sarutobi
-------------------------------

Anche se l'impresa di preparare una lista esaustiva delle serie TV e
film in cui compare Sasuke Sarutobi è senza speranza, mi pare comunque
il caso di citare qualche esempio, per guidare chi volesse saperne di
più sul celebre ninja.

Innanzitutto citerò una serie che viene spesso confusa con 
*Sasuke il piccolo ninja", per lo meno in Italia, a causa di un titolo
quasi identico: sto parlando di [Sasuke, il
piccolo guerriero](https://www.animeclick.it/anime/2436/il-piccolo-guerriero)
del 1979, noto in occidente come *Ninja the Wonder Boy*. I due anime
sono diversi come il giorno e la notte: "Il piccolo guerriero" è molto
più leggero e solare, privo delle atmosfere di tragedia tipiche delle
classiche opere sui ninja. Rimane comunque una serie d'azione ambientata
nel Giappone medievale.

![piccolo-guerriero](piccolo-guerriero.jpg)

Se volete qualcosa di davvero demenziale associato al nome Sarutobi,
rimanendo negli anni settanta consiglio la serie *Sarutobi Ecchan* (in
Italia [Hela
Supergirl](https://www.animeclick.it/anime/1608/hela-supergirl)) del
1971, tratta da un manga shojo umoristico nientepopodimeno che di
Shotaro Ishinomori, che di opere sui ninja ne ha scritte un bel
po'. Hela è una bambina contemporanea (ovvero della fine degli anni
sessanta) che va regolarmente a scuola vivendo una vita normale; le
sue di qualità eccezionali, in quanto discendente del solito Sasuke
Sarutobi, sono sfruttate puramente a scopo umoristico. Purtroppo nella
versione italiana il sottotesto ninja è stato completamente perso, ma
la serie fa comunque divertire e la fortissima Hela si può considerare
un antesignana della più celebre Arale.

![hela](hela.png)

Secondo la tradizione classica Sasuke era al servizio del generale
Sanada Yukimura (vero nome Sanada Nobushige, 1567-1615), assieme ad
altri nove ninja leggendari. La serie TV non parla di ciò, visto è
ambientata vent'anni dopo l'assedio di Osaka, ma non è difficile
trovare opere che ne parlano, a partire dal film Toei del 1959 e dal
manga di Tezuka del 1960, fino ad arrivare al recente manga [Brave
10](https://www.animeclick.it/manga/10012/brave-10) di Kairi
Shimotsuki da cui è stata tratta una [serie anime
omonima](https://www.animeclick.it/anime/3484/brave-10) nel 2012. Se
volete qualcosa di ancora più recente c'è il film del 2016 intitolato
appunto per l'appunto *Sanada Ten Braves*, che è un live action ma i
primi dieci minuti sono di animazione.

I ninja non passano mai di moda e quindi è sempre possible trovare in
ogni momento citazioni e omaggi alle antiche leggende. Gli anime a
tema ninja ne sono ovviamente pieni: tanto per fare un esempio, la
rivalità tra Sasuke Uchiya e Naruto Uzumaki è un eco di quella
leggendaria tra Sasuke Sarutobi e Saizo Kirigakure, un altro dei Dieci
di Sanada. Quello che può essere meno ovvio è che anche in serie che
non sembrano c'entrare nulla con l'antico Giappone non è difficile
trovare citazioni all'immaginario ninja, come per esempio in "Ninja
Senshi Tobikage" del 1985, curiosa commistione tra ninja e super
robot.

![tobikage](tobikage.jpg)

Del resto l'accoppiata tra ninja e alieni era già stata
azzeccata da un giovanissimo Go Nagai nei primi anni sessanta, quando
era ancora un autore sconosciuto le cui opere venivano rifiutate. Sto
parlando del suo manga "Leone Nero" che riuscì a pubblicare soltanto
nel 1978, quando era mai un autore affermato. Il manga inizia come un
clone dei romanzi di Yamada, con i ninja di Koga e Iga, continua con
robot ninja immortale che anticipa il Terminator di Schwarzenegger di
vent'anni e termina con un'invasione aliena, astronavi e viaggi nel
tempo! Non ci si fa mancare nulla!

![blacklion](blacklion.jpg)

Approfondimenti
---------------

Per la situazione storica del Giappone subito dopo la seconda guerra
mondiale e negli anni dell'ascesa del gekiga e il primo boom dei ninja
consiglio il libro di Karyn Poupée, "Histoire du Manga",
ed. "Tallandier".

Per la vera storia dei ninja consiglio "In search of the Ninja",
sottotitolo "The historical truth of ninjutsu" di Antony Cummins,
ed. "The History Press".  Sullo stesso tema, ma più recente, c'è anche
"Ninja: Unmasking the Myth" di Steven Turnbull, ed. Pen & Sword Books,
che però non ho letto.

Se siete interessanti al film di Sasuke del 1959, Mario Rumor gli dedica
il quinto capitolo del suo libro *Toei Animation*, ed. Cartoon Club.

Il romanzo fondamentale sui ninja è [Kouga Ninpocho](
https://www.animeclick.it/novel/21031/kouga-ninpouchou) di Futaro Yamada,
che si può leggere in inglese sotto il titolo "The Kouga Ninja
Scrolls", ed. "Del Rey". Ci sono almeno [25 film live action derivati
dalle opere di
Yamada](https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/List_of_ninja_films.html),
realizzati tra il 1963 e il 2011, per non parlare di manga e anime.
Se avete visto "Basilisk" sapete già tutto.

Il manga fondamentale sui ninja è [Ninja Bugeicho](https://www.animeclick.it/manga/11888/kagemaru-den-la-leggenda-di-un-ninja) di Sanpei
Shirato. In Giappone è un cult, un'opera che divenne simbolo della
contestazione giovanile degli anni sessanta, esattamente come Ashita
no Joe. Fortunamentente si può trovare anche in Italia, dove è stato
edito dalla Hazard nel 2012 in quattro volumi giganti, sotto il titolo
"Kagemaru Den - la leggenda di un ninja. Il [film di Ninja
Bugeicho](https://www.youtube.com/watch?v=c0Dvi80axlA) si trova su
YouTube; si tratta di un film sperimentale realizzato semplicemente
filmando le immagini del manga e aggiungendoci il sonoro.

Ancora più famosa è la successiva saga di [Kamui](
https://www.animeclick.it/anime/3484/brave-10) su cui Shirato ha
continuato a lavorare per tutta la vita a partire dal 1964.

Altri manga e anime su Sasuke Saritobi si posso trovare facilmente con una
ricerca su AnimeClick o in rete. Sfortunatamente il manga originale di Sasuke,
in quindici volumi, è ancora introvabile in lingua occidentale.
