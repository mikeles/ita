TENGEN TOPPA GURREN LAGANN
===========================================

*Ci sono delle serie con delle scene tamarre; ci sono delle serie
profondamente tamarre; ci sono delle serie a livello di tamarrata
galattica; oltre tutto questo, al di la' di ogni limite, c'e' Gurren
Lagann.*

LA TAMARRAGINE
----------------------------

Gurren Lagann e' l'apoteosi della tamarrata, della smargiassata,
dell'esagerazione al di la' di ogni decenza e del buon senso, la serie
piu' eccessivamente tamarra che sia mai stata realizzata e
probabilmente e' destinata a restare insuperata anche nel
futuro. Personalmente io sono sempre stato contrario ai personaggi
tamarri e smargiassi, ma Gurren Lagann riesce a raffinare cosi'
all'inverosimile l'arte della tamarrata da diventare degno di
attenzione e di interesse.

Ammetto che qualche volta mi sbaglio nel giudicare una serie dalle
prime puntate, ma mai mi sno sbagliato cosi' tanto come in Gurren
Lagann. Ho visto i primi sei o sette episodi in sei mesi, tra mille
sbadigli e un grosso fastido, chiedendomi piu' volte cose ci
vedesse la gente in questa serie che trovavo pessima e ho pensato
anche seriamente di sospenderne la visione. Fortunatemente la
mia testardaggine nel voler portare a termine le serie che inizio
mi e' venuta in aiuto; vista l'ottava puntata la situazione ha
cominciato a sbloccarsi e ho iniziato a vedere la serie con
occhi nuovi. Ci sono volute alcune settimane e 7-8 episodi per
farmi cambiare idea, ma alla fine mi sono convinto
ed ho visto gli ultimi 11 episodi tutti d'un fiato in un solo
giorno: questo e' il potere di Gurren Lagan!!

IL RITORNO DEI ROBOTTONI
-----------------------------------------

Perche' Gurren Lagann ha avuto cosi' tanto successo, cosi' tanto seguito ed
e' stato accolto cosi' positivamente? Secondo me c'e' una ragione fondamentale:
perche' e' un robotico <i>vero</i>.  

Notate bene che dico robotico, non un mecha alla Gundam, ma un vero
anime stile Go Nagai dei primi anni settanta. Il robotico e' un genere
che dopo lunghissima malattia sembrava essere stato ucciso
definitivamente dalla Gainax nel 1995 con Evangelion (cosa per cui ho
sempre serbato rancore allo studio, sia detto per inciso) ma che e'
risorto dalle sue ceneri nel 2007 proprio grazie a <i>Sfondamento dei
cieli Gurren Lagann</i>, della stessa Gainax. In tutti questi anni (il
genere robotico era gia' moribondo ben prima di Evangelion) c'e' stato
il tempo per formare una nuova generazione di fan che non ha mai visto
il robotico classico, o non l'ha hai visto in una forma che potesse
digerire. Ed e' proprio questo quello che Gurren Lagann e' riuscito a
fare: prendere le tematiche piu' pure del robotico classico,
aggiungere una forte dose di ironia e fanservice e proporle in una
forma digeribile al pubblico moderno. La forza di Gurren Lagann sta
nell'avere la parvenza della parodia del robotico classico, senza
esserlo nel profondo: anzi nel profondo aderisce alla filosofia piu'
tradizionale del genere, fondato sul <i>sense of wonder</i>, le
trasformazioni, gli agganciamenti, le epiche battaglie spaziali, i
combattimenti tra robot giganti, i nemici coloriti e improponibili, la
predominanza del sentimento sulla ragione, il coraggio, lo spirito di
sacrificio, la guerra, la morte, l'avventura, la crescita, lo spirito
di corpo, l'amicizia, l'amore negato, la tragedia e la vittoria.

Tutto questo e molto di piu' e' Gurren Lagann, ma tutto questo erano
anche i cartoni (allora si chiamavano cosi') di Go Nagai. La
differenza e' nella realizzazione tecnica e nel target di pubblico:
allora erano il target erano i bambini, adesso il target sono gli
adolescenti, motivo per cui Gurren Lagann non lesina buone dosi di fan
service, a scopo per lo piu' umoristico e per alleggerire una storia
che si svolge sul registro del grottesco, alternando comicita' e
avventura a riflessione e tragedia. In Gurren Lagann infatti c'e'
anche riflessione, non tanto dei personaggi, ma dello spettatore che
segue la serie: si tratta di una serie molto piu' sofisticata
intellettualmente di quello che poteva essere un robotico classico. A
parte questo, la differenza piu' grande con il passato sta
nella mentalita' del pubblico giapponese, che e' molto cambiata negli ultimi
quarant'anni, per cui quello che una volta si poteva dire seriamente
adesso va detto in tono grottesco, facendolo sembrare uno scherzo. E'
per questo che i dialoghi di Gurren Lagann sono volutamente esagerati,
ma restano impressi nello spettatore perche' al di la' della forma
(parodica del robotico classico) la sostanza rimane la stessa:
<i>credi in me perche' io credo in te che credi in me</i>, <i>con la tua
trivella puoi sfondare anche il cielo, Simon</i>, <i>dai un calcio
alla ragione e fai posto all’impossibile</i>.

Il mondo di Gurren Lagann e' lo stesso mondo dell'anime robotico
pre-Gundam, in cui la volonta' permette di fare qualunque cosa, e non
esistono limiti imposti dalla ragione o dalla fisica. Gurren Lagann e'
un ritorno al passato, una serie che colpisce lo spettatore nelle sue
corde piu' irrazionali e infantili: anche se e' pensato per un
pubblico adolescente, e' una serie adattissima anche ai bambini. Gli
aspetti infantili sorprendentemente coesistono con una trama 
articolata, estremamente originale e sorprendente. Sono inoltre presenti,
accanto a personaggi tamarrissimi come Kamina, Kittan e la maggior
parte dei membri della Brigata Gurren,
anche dei personaggi razionali, in primis Rossiu e Lord Genome che
non a caso si alleano. Inoltre esiste tutta una simbologia sottostante
la serie, quella della spirale: il potere di Gurren Lagann e'
l'infinito potere della Spirale, che si identifica con il potere
dell'evoluzione e della Vita; d'altra parte viene detto che tale
potere non e' affatto positivo ma e' affine ad un cancro incontrollato
che va estirpato dall'Universo. Gurren Lagann e' si' una serie leggera
e senza pretese, ma nello stesso tempo sono presenti delle sottotrame
inaspettatamente profonde (si pensi al rapporto di Rossiu con il suo
maestro, la storia del Libro sacro e in generale gli aspetti
religiosi) e dei personaggi che fanno riflettere.  Il tutto e' immerso
in una trama estremamente ben pensata: Gurren
Lagann inizia sottoterra, continua sopra la Terra in stile robotico
classico con combattimenti contro i quattro generali, diventa
improvvisamente una space opera in stile Corazzata Spaziale Yamato e
il finale di proporzioni cosmiche e' al di fuori di qualunque schema e
completamente imprevedibile nella sua escalation illimitata. E'
soltanto nella ventisettesima e ultima puntata che capiremo il
vero significato del titolo e vedremo per la prima volta
il Tengen Toppa Gurren Lagann, la versione del Gurren Lagann che e'
*letteralmente* in grado di sfondare i cieli.

IL GENIO
----------------------------------

C'e' del genio in Gurren Lagann.
Nel 1972 Go Nagai ha l'idea dell'agganciamento, un meccanismo
per cui un normale ragazzo sfruttando un piccolo modulo di controllo
volante (il pilder) puo' controllare un gigantesco robot. Nel
2007 Gurren Lagann prende questa stessa idea e la porta alle
sue estreme conseguenze, arrivando la' dove nessuno aveva mai
immaginato di arrivare in 35 anni di anime robotici basati sull'idea
dell'agganciamento. Anche il crescendo che e' il tratto caratteristico
tipico di Gurren Lagann non ha precedenti nella storia dell'animazione
giapponese. Nel 1989 ci aveva provato Hideaki Anno con Gunbuster, una
serie di ottima qualita' che ha detenuto per 18 anni il record di
serie con il piu' grande crescendo della storia del genere robotico:
ma Gurren Lagann riduce a nulla il crescendo di Gunbuster. Anche perche'
Gurren Lagann parte da molto piu' in basso, da serie parodica del
genere robotico, fino a diventarne invece il piu' grande rappresentante
degli ultimi decenni. 

OREINTE E OCCIDENTE
-----------------------------------------------

Manga e anime come li conosciamo oggi non sarebbero mai esistiti senza
l'influsso dell'Occidente, tanto e' vero che il Dio dei Manga Osamu
Tezuka ha sempre riconosciuto pubblicamente come suo maestro e
ispiratore Walt Disney. Nel corso degli anni l'influenza
dell'Occidente su manga e anime e in genere sulla societa' giapponese
non e' certo diminuita, e Gurren Lagann e' un perfetto esempio di
opera giapponese moderna fortemente occidentalizzata: questo lo si
vede nella colonna sonora, nel ritmo frenetico, nei colori, nelle
esplosioni, nei personaggi chiassosi ed eccessivi e un po'
dappertutto.  Anzi, Gurren Lagann e' tal punto occidentalizzato che
viste le prime puntate l'ho sdegnosamente classificato tra le
*americanate*. Personalmente ho sempre fatto una distinzione enorme
fra la tipica opera d'azione americana e il cartone animato giapponese
classico: la prima si contraddistingue per la superficialita', la
scontatezza, il lieto fine obbligatorio, la facilita' con cui i
protagonisti superano tutti gli ostacoli e vincono i nemici senza che
nessuno si faccia mai male (al massimo puo' morire eroicamente il
personaggio gia' avanti con gli anni, che non avrebbe nulla di buono
da fare comunque).  L'anime e il manga giapponese invece, e qui
riferisco agli esempi classici del genere, precedenti gli anni
ottanta, si contraddistingue per una forte drammaticita', un finale
non scontato (puo' essere positivo ma spesso e volentieri puo' essere
anche tragico) e soprattutto la fatica immane dei protagonisti, che
puo' essere ricompensata o anche no.  In particolare, pensate a opere
sportive come Ashita no Joe, l'Uomo Tigre, ma anche Mimi' e la
nazionale della pallavolo, o anche Grand Prix e il Campionissimo: la
vittoria per questi eroi non e' scontata, possono perdere e anche
quando vincono c'e' un prezzo elevatissimo da pagare. Paragonate
queste opere a film americani come Karate Kid in cui un qualunque
ragazzino puo' diventare un campione di arti marziali con pochi
giorni di allenamento: i film americani vi parranno ridicoli.



LE CITAZIONI
-----------------------------------------------

Se volessi discuture tutte le citazioni a serie classiche (robotiche e non) 
presenti in Gurren Lagann dovrei scrivere un libro, tanto queste sono
onnipresenti e pervasive. Ho notato che chiunque ha visto Gurren Lagann
ha trovato riferimenti agli anime della sua generazione. Io sono della
prima generazione del robotico, cresciuto con Goldrake e Mazinga ed
evidentemente sono piu' sensibile a questo tipo di citazioni, che comunque
sono quelle piu' significative, perche' di prima mano. Molti per esempio
hanno visto rimandi a Gundam G in Gurenn Lagann, ma questo e' un riferimento
indiretto, perche' a sua volta Gundam G pesca dal robotico classico che e'
stato il primo a introdurre le idee che vediamo in Gurren Lagann. In
questo paragrafo mi limitero' a segnalare solo alcune delle citazioni
piu' evidenti, nel mare magnum che e' Gurren Lagann: a volte invidio
i giovani che possono guardarsi questa serie a mente fresca, senza
essere distratti dalle decine e decine di rimandi a serie del passato,
come succede ad uno spettatore come me, che ha decine e decine di
anime robotici in memoria.

Cominciamo con ordine. In primo luogo i robot degli uomini bestia, i Gunman,
che hanno la testa sul corpo, sono un chiaro omaggio ai mostri del Grande
Mazinga di Go Nagai (1974). Le atmosfere di Gurren Lagann, cosi', come le
espressioni facciali dei robot, sono una chiara citazione della prima
serie robotica umoristica e tamarra, il Daitarn III (1978). La mezzaluna
sulla fronte del Gurren Lagann fa tornare in mente Zambot III (1977)
di Tomino, l'autore di Gundam. Le trivelle non possono fare altro che
richiamare il Getter Robot (1974), sempre di Go Nagai, il primo caso
nella storia del robotico in cui uno dei piloti muore in battaglia
e continua a essere ricordato per tutte le puntate successive.
Oltre alle citazioni al Go Nagai piu' classico, Gurren Lagann cita
pesantemente anche il Matsumoto di Capitan Harlock e soprattutto quello
della Corazzata Spaziale Yamato (arrivato in Italia nella versione
americana come Starblazers). Cos'e' il Dai Gurren se non una versione
con braccia e gambe della Yamato? E anche le atmosfere della seconda
parte della serie sono evidentamente tratte dalla space opera di
Matusmoto: questo e' particolarmente visibile nell'abbigliamento
dei personaggi, nel mantello alla Capitan Harlock sfoggiato inizialmente
da Kamina e poi da Simon. Anche molti dei discorsi di Gurren Lagann
sembrano parodie dei discorsi di Harlock: sono che Harlock era serio,
taciturno, triste e tormentato, mentre Kamina e' tamarro, caciarone,
allegro ed eccessivo in tutto. Lo spirito di Matsumoto viene totalmente
stravolto, trattato con una assoluta mancanza di rispetto e sfottuto
all'inverosimile: eppure, sotto sotto, la genialita' di Matsumoto viene
riconosciuta e nelle battaglie spaziali il lettore meno giovane
vedra' echi degli epici combattimenti dell'Arcadia e della Yamato
(o dell'Alcadia e dell'Argo, come le chiamavamo ai tempi).


Gainax cita se' stessa, soprattutto nella sesta puntata (quella delle
terme).

I MESSAGGI
-------------------------------------

Molti hanno visto dei messaggi profondi in Gurren Lagann, sotto la
patina scherzosa e caciarona. E' in dubbio che si tratta di una serie
piu' sofisticata di quanto possa apparire a prima vista, con un forte
contenuto simbolico: la Terra (madre e prigione), il Cielo (Il limite
da sfondare), la Trivella (la volonta' umana) sono chiaramente dei
simboli, e la simbologia della Spirale e' esplicita in tutta la serie,
anche nell'impianto narrativo: con lo scorrere delle puntate
l'ambientazione si allarga a spirale, dal piccolo villaggio
sotterraneo, alla superficie, all'intero pianeta Terra, alla Galassia,
all'intero multiuniverso negli ultimi episodi.

L'intero anime è caratterizzato da una continua lotta fra razionalità
e irrazionalità E' insomma una realtà che impedisce, a lui come agli
altri uomini, di vedere quella vera: il cielo azzurro e la superficie,
per la cui conquista occorre lottare e compiere un difficoltoso
percorso - Mayu SOS

 che tutta la nostra esistenza possa essere in mano ad un destino
 predefinito è un pensiero che almeno una volta nella vita tutti hanno
 formulato. -- Alucard frasi e discorsi carismatici e coinvolgenti, ma
 che il più delle volte sono assolutamente privi di significato; --
 horus

 l'unico personaggio che usa la logica (Rossiu), senza usare il cuore,
 prenderà le decisioni sbagliate.character design di Atsushi Nishigori
 --mifune

I Gunman si pilotano con la forza di volonta' e lo spirito combattivo.

E' giusto credere che tutto sia possibile, che non ci siano mai limiti
o punti d'arrivo, e che il nostro avvenire ce lo creiamo noi stessi?
-- deathmetalsoul

CONCLUSIONE
----------------------------------

Per me il merito piu' grande del Gurren Lagann e' quello di aver fatto
conoscere ad una nuova generazione la grandezza del robotico classico
e della space opera classica, che erano andate dimenticate. Il segreto
e' stato riprorre le vecchie in una forma moderna e accattivamente.
Ho visto innumerevoli giovani dire "il robotico non mi e' mai
piaciuto, ma Gurren Lagann ..." senza rendersi conto che quello che a
loro non piaceva del robotico era soltanto la forma esterna, da loro
giudicata datata: il nucleo centrale del robotico, ovverossia il sense
of wonder, le trasformazioni e l'epicita' piacevano trent'anni fa come
piaccione adesso e Gurren Lagann l'ha dimostrato. E' riuscito a far
credere a un'intera generazione di trovarsi di fronte a innovazioni
incredibili. Ho visto molti giovani esaltarsi vedendo personaggi che
si sacrificano per il bene comune e muoiono eroicamente perche' per
loro questa era una cosa assolutamente innovativa, abituati
all'animazione moderna che si riduce quasi esclusivamente alla
commedia scolastica. Per uno come me, cresciuto con gli anime degli
anni settanta, questa invece e' una cosa normale del genere.
Anche il finale, sorprendente rispetto agli standard moderni, e'
assolutamente consistente rispetto ai finali classici del
genere robotico (mi vengono in mente i finali di Goldrake
o di Daitarn III) o del film western.
l'anti-Evangelion.

Le musiche sono adatte alla serie.
Chara design eccellente. Mecha design eccellente.
'assenza di cognomi,  a colori più freddi, ad ambientazioni notturne o scure, ma ad un altrettanto bello coinvolgimento emotivo.

esobiologia -  buoni vincono i cattivi perdono?

americanata?

Yoko-maestra

Gaiking

TUTTO incredibilmente fatto con cura. Nulla è lasciato al caso.--Danteale83

. Una bella fine, non credevo si potesse fare di meglio nella secondo parte, invece mi sono sbagliato ancora. -- Holly81

apace di emozionare davvero, di farti saltare dalla sedia, -- Godai

la talpa, Boota
